<?php

class bt_bb_inner_step extends BT_BB_Element {

	function handle_shortcode( $atts, $content ) {
		extract( shortcode_atts( apply_filters( 'bt_bb_extract_atts', array(
			'title' 	=> '',
			'html_tag'      => 'h3',
			'text' 		=> '',
			'image' 	=> '',
			'highlight'     => '',
			'top_margin'    => '',
		) ), $atts, $this->shortcode ) );
		
		$title = html_entity_decode( $title, ENT_QUOTES, 'UTF-8' );
		
		$class = array( $this->shortcode );

		if ( $el_class != '' ) {
			$class[] = $el_class;
		}

		$id_attr = '';
		if ( $el_id != '' ) {
			$id_attr = ' ' . 'id="' . esc_attr( $el_id ) . '"';
		}                
                
                if ( $top_margin != '' ) {
                        $el_style = $el_style . 'margin-top:' . $top_margin . ';';
		}

		$style_attr = '';
		if ( $el_style != '' ) {
			$style_attr = ' ' . 'style="' . esc_attr( $el_style ) . '"';
		}
                
		if ( $highlight != '' ) {
			$class[] = $this->prefix . 'highlight';
		}

		$class = apply_filters( $this->shortcode . '_class', $class, $atts );

		$output = '';
		$output .= '<div' . $id_attr . ' class="' . implode( ' ', $class ) . '"' . $style_attr . '>';

			
			$output .= '<div class="' . esc_attr( $this->shortcode ) . '_content bt_bb_animation_fade_in animate">';
				$output .= '<div class="' . esc_attr( $this->shortcode ) . '_content_holder"><div class="' . esc_attr( $this->shortcode ) . '_content_holder_sleeve">';
					if ( $title != '' ) $output .= '<'. $html_tag .' class="' . esc_attr( $this->shortcode ) . '_title">' . $title . '</' . $html_tag . '>';
					if ( $text != '' ) $output .= '<div class="' . esc_attr( $this->shortcode ) . '_text">' . $text . '</div>';
					$output .= '<div class="' . esc_attr( $this->shortcode ) . '_inner_content">';
						if ( $content != '' ) $output .=  wpautop( wptexturize( do_shortcode( $content ) ) );
					$output .= '</div></div>';
				$output .= '</div>';
			$output .= '</div>';

			
			if ( $image != '' ) $output .=  '<div class="' . esc_attr( $this->shortcode . '_image bt_bb_animation_fade_in animate') . '">' . do_shortcode( '[bt_bb_image image="' . esc_attr( $image ) . '" size="medium"]' ) . '</div>';

		$output .= '</div>';

		$output = apply_filters( 'bt_bb_general_output', $output, $atts );
		$output = apply_filters( $this->shortcode . '_output', $output, $atts );
		
		return $output;

	}

	function map_shortcode() {
		bt_bb_map( $this->shortcode, array( 'name' => esc_html__( 'Timeline Event', 'codiqa' ), 'description' => esc_html__( 'Inner step element', 'codiqa' ), 'as_child' => array( 'only' => 'bt_bb_step_line' ), 'container' => 'vertical', 'accept' => array( 'bt_bb_headline' => true, 'bt_bb_text' => true, 'bt_bb_button' => true, 'bt_bb_icon' => true, 'bt_bb_separator' => true ), 'icon' => $this->prefix_backend . 'icon' . '_' . $this->shortcode,
			'params' => array(
				array( 'param_name' => 'title', 'type' => 'textfield', 'heading' => esc_html__( 'Title', 'codiqa' ), 'preview' => true ),
				array( 'param_name' => 'html_tag', 'type' => 'dropdown', 'default' => 'h3', 'heading' => esc_html__( 'HTML tag', 'codiqa' ),
					'value' => array(
						esc_html__( 'h1', 'codiqa' ) => 'h1',
						esc_html__( 'h2', 'codiqa' ) => 'h2',
						esc_html__( 'h3', 'codiqa' ) => 'h3',
						esc_html__( 'h4', 'codiqa' ) => 'h4',
						esc_html__( 'h5', 'codiqa' ) => 'h5',
						esc_html__( 'h6', 'codiqa' ) => 'h6'
				) ),
				array( 'param_name' => 'text', 'type' => 'textarea', 'heading' => esc_html__( 'Text', 'codiqa' ) ),
				array( 'param_name' => 'image', 'type' => 'attach_image', 'heading' => esc_html__( 'Image', 'codiqa' ) ),
				array( 'param_name' => 'top_margin', 'type' => 'textfield', 'heading' => esc_html__( 'Top Margin', 'codiqa' ), 'description' => esc_html__( 'Use to overlap odd and even items without image, not applicable when timeline is responsive.', 'codiqa' ), 'preview' => true ),
				array( 'param_name' => 'highlight', 'type' => 'checkbox', 'value' => array( esc_html__( 'Yes', 'codiqa' ) => 'show_highlighted' ), 'heading' => esc_html__( 'Show as highlighted', 'codiqa' ), 'description' => esc_html__( 'Adds a gradient background, accent to alternate and turns text into white.', 'codiqa' ), 'preview' => true,
				)
			)
		) );
	}
}