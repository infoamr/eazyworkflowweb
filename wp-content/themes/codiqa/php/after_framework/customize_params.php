<?php

if ( ! function_exists( 'boldthemes_customize_heading_style' ) ) {
	function boldthemes_customize_heading_style( $wp_customize ) {
		
		$wp_customize->add_setting( BoldThemesFramework::$pfx . '_theme_options[heading_style]', array(
			'default'           => BoldThemes_Customize_Default::$data['heading_style'],
			'type'              => 'option',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field'
		));
		$wp_customize->add_control( 'heading_style', array(
			'label'     => esc_html__( 'Heading Style', 'codiqa' ),
			'section'   => BoldThemesFramework::$pfx . '_typography_section',
			'settings'  => BoldThemesFramework::$pfx . '_theme_options[heading_style]',
			'priority'  => 100,
			'type'      => 'select',
			'choices'   => array(
			'default' => esc_html__( 'Default', 'codiqa' ),
			'compact' => esc_html__( 'Compact (small line height + bold)', 'codiqa' )
			)
		));
	}
}
add_action( 'customize_register', 'boldthemes_customize_heading_style' );

// BUTTONS FONT

if ( ! function_exists( 'boldthemes_customize_button_font' ) ) {
	function boldthemes_customize_button_font( $wp_customize ) {
		
		$wp_customize->add_setting( BoldThemesFramework::$pfx . '_theme_options[button_font]', array(
			'default'           => urlencode( BoldThemes_Customize_Default::$data['button_font'] ),
			'type'              => 'option',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'boldthemes_sanitize_select'
		));
		$wp_customize->add_control( 'button_font', array(
			'label'     => esc_html__( 'Button Font', 'codiqa' ),
			'section'   => BoldThemesFramework::$pfx . '_typo_section',
			'settings'  => BoldThemesFramework::$pfx . '_theme_options[button_font]',
			'priority'  => 99,
			'type'      => 'select',
			'choices'   => BoldThemesFramework::$customize_fonts
		));
	}
}
add_action( 'customize_register', 'boldthemes_customize_button_font' );
add_action( 'boldthemes_customize_register', 'boldthemes_customize_button_font' );

// CUSTOMIZE MENU FONT

if ( ! function_exists( 'boldthemes_customize_menu_font_size' ) ) {
	function boldthemes_customize_menu_font_size( $wp_customize ) {
		
		$wp_customize->add_setting( BoldThemesFramework::$pfx . '_theme_options[menu_font_size]', array(
			'default'           => BoldThemes_Customize_Default::$data['menu_font_size'],
			'type'              => 'option',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'boldthemes_sanitize_select'
		));
		$wp_customize->add_control( 'menu_font_size', array(
			'label'     => esc_html__( 'Menu Font Size', 'codiqa' ),
			'section'   => BoldThemesFramework::$pfx . '_typo_section',
			'settings'  => BoldThemesFramework::$pfx . '_theme_options[menu_font_size]',
			'priority'  => 100,
			'type'      => 'select',
			'choices'   => array(
				'12'  	=> esc_html__( '12px', 'codiqa' ),
				'13'  	=> esc_html__( '13px', 'codiqa' ),
				'14'  	=> esc_html__( '14px', 'codiqa' ),
				'15'  	=> esc_html__( '15px', 'codiqa' ),
				'16'  	=> esc_html__( '16px', 'codiqa' ),
			)
		));
	}
}
add_action( 'customize_register', 'boldthemes_customize_menu_font_size' );
add_action( 'boldthemes_customize_register', 'boldthemes_customize_menu_font_size' );

// SHOP SALE TAG DESIGN
if ( ! function_exists( 'boldthemes_customize_shop_sale_tag_design' ) ) {
	function boldthemes_customize_shop_sale_tag_design( $wp_customize ) {
		
		$wp_customize->add_setting( BoldThemesFramework::$pfx . '_theme_options[shop_sale_tag_design]', array(
			'default'           => BoldThemes_Customize_Default::$data['shop_sale_tag_design'],
			'type'              => 'option',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'boldthemes_sanitize_select'
		));
		$wp_customize->add_control( 'shop_sale_tag_design', array(
			'label'     => esc_html__( 'Shop Sale Tag Design', 'codiqa' ),
			'section'   => BoldThemesFramework::$pfx . '_shop_section',
			'settings'  => BoldThemesFramework::$pfx . '_theme_options[shop_sale_tag_design]',
			'priority'  => 98,
			'description' => esc_html__( 'Choose a design for shop sale tag.', 'codiqa' ),
			'type'      => 'select',
			'choices'   => array(
			'hard-rounded' => esc_html__( 'Hard Rounded', 'codiqa' ),
			'hard-rounded-top-left' => esc_html__( 'Hard Rounded except top left corner', 'codiqa' ),
			'hard-rounded-top-right' => esc_html__( 'Hard Rounded except top right corner', 'codiqa' ),
			'hard-rounded-bottom-left' => esc_html__( 'Hard Rounded except bottom left corner', 'codiqa' ),
			'hard-rounded-bottom-right' => esc_html__( 'Hard Rounded except bottom right corner', 'codiqa' ),
			'soft-rounded' => esc_html__( 'Soft Rounded', 'codiqa' ),
			'soft-rounded-top-left' => esc_html__( 'Soft Rounded except top left corner', 'codiqa' ),
			'soft-rounded-top-right' => esc_html__( 'Soft Rounded except top right corner', 'codiqa' ),
			'soft-rounded-bottom-left' => esc_html__( 'Soft Rounded except bottom left corner', 'codiqa' ),
			'soft-rounded-bottom-right' => esc_html__( 'Soft Rounded except bottom right corner', 'codiqa' ),
			'square' => esc_html__( 'Square', 'codiqa' )
			)
		));	
	}
}
add_action( 'customize_register', 'boldthemes_customize_shop_sale_tag_design' );
add_action( 'boldthemes_customize_register', 'boldthemes_customize_shop_sale_tag_design' );

// CUSTOM 404 IMAGE
if ( ! function_exists( 'boldthemes_customize_image_404' ) ) {
	function boldthemes_customize_image_404( $wp_customize ) {
		
		$wp_customize->add_setting( BoldThemesFramework::$pfx . '_theme_options[image_404]', array(
			'default'           => BoldThemes_Customize_Default::$data['image_404'],
			'type'              => 'option',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'boldthemes_sanitize_image'
		));
		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'image_404', array(
			'label'    => esc_html__( 'Custom Error 404 Page Image', 'codiqa' ),
			'section'  => BoldThemesFramework::$pfx . '_general_section',
			'settings' => BoldThemesFramework::$pfx . '_theme_options[image_404]',
			'priority' => 121,
			'context'  => BoldThemesFramework::$pfx . '_image_404'
		)));
	}
}
add_action( 'customize_register', 'boldthemes_customize_image_404' );
add_action( 'boldthemes_customize_register', 'boldthemes_customize_image_404' );

// Show logo and logo widgets
if ( ! function_exists( 'boldthemes_customize_show_logo_and_logo_widgets' ) ) {
	function boldthemes_customize_show_logo_and_logo_widgets( $wp_customize ) {
		
		$wp_customize->add_setting( BoldThemesFramework::$pfx . '_theme_options[show_logo_and_logo_widgets]', array(
			'default'           => BoldThemes_Customize_Default::$data['show_logo_and_logo_widgets'],
			'type'              => 'option',
			'capability'        => 'edit_theme_options',
			'sanitize_callback' => 'boldthemes_sanitize_checkbox'
		));
		$wp_customize->add_control( 'show_logo_and_logo_widgets', array(
			'label'    => esc_html__( 'Show logo and logo widgets on sticky header when menu is below logo', 'codiqa' ),
			'section'  => BoldThemesFramework::$pfx . '_header_footer_section',
			'settings' => BoldThemesFramework::$pfx . '_theme_options[show_logo_and_logo_widgets]',
			'priority' => 61,
			'type'     => 'checkbox'
		));	
	}
}
add_action( 'customize_register', 'boldthemes_customize_show_logo_and_logo_widgets' );
add_action( 'boldthemes_customize_register', 'boldthemes_customize_show_logo_and_logo_widgets' );

