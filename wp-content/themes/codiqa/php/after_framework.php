<?php

BoldThemes_Customize_Default::$data['body_font'] = 'Open Sans';
BoldThemes_Customize_Default::$data['heading_supertitle_font'] = 'Open Sans';
BoldThemes_Customize_Default::$data['heading_font'] = 'Raleway';
BoldThemes_Customize_Default::$data['heading_subtitle_font'] = 'Open Sans';
BoldThemes_Customize_Default::$data['menu_font'] = 'Raleway';
BoldThemes_Customize_Default::$data['button_font'] = 'Raleway';
BoldThemes_Customize_Default::$data['menu_font'] = 'Raleway';
BoldThemes_Customize_Default::$data['menu_font_size'] = 'menu-font-size-13';
BoldThemes_Customize_Default::$data['buttons_shape'] = 'hard-rounded-top-left';

BoldThemes_Customize_Default::$data['accent_color'] = '#fb17ce';
BoldThemes_Customize_Default::$data['alternate_color'] = '#210d94';
BoldThemes_Customize_Default::$data['logo_height'] = '120';
BoldThemes_Customize_Default::$data['box_menu_content'] = false;

BoldThemes_Customize_Default::$data['template_skin'] = 'light';
BoldThemes_Customize_Default::$data['heading_style'] = 'default';

// GENERAL
BoldThemes_Customize_Default::$data['shop_sale_tag_design'] = 'hard-rounded-top-left';
BoldThemes_Customize_Default::$data['image_404'] = get_template_directory_uri() . '/gfx/404.jpg'; 

BoldThemes_Customize_Default::$data['show_logo_and_logo_widgets'] = false;

// Match theme styles by default
BoldThemes_Customize_Default::$data['supertitle_position'] = true;
BoldThemes_Customize_Default::$data['menu_type'] = 'horizontal-left';
BoldThemes_Customize_Default::$data['boxed_menu'] = false;
BoldThemes_Customize_Default::$data['sticky_header'] = true;
BoldThemes_Customize_Default::$data['sidebar_use_dash'] = true;
BoldThemes_Customize_Default::$data['menu_font_size'] = '13';
BoldThemes_Customize_Default::$data['blog_grid_gallery_columns'] = '3';
BoldThemes_Customize_Default::$data['blog_grid_gallery_gap'] = 'normal';
BoldThemes_Customize_Default::$data['blog_list_view'] = 'standard';
BoldThemes_Customize_Default::$data['blog_single_view'] = 'standard';
BoldThemes_Customize_Default::$data['blog_author'] = true;
BoldThemes_Customize_Default::$data['blog_date'] = true;
BoldThemes_Customize_Default::$data['blog_side_info'] = true;
BoldThemes_Customize_Default::$data['blog_author_info'] = true;
BoldThemes_Customize_Default::$data['blog_use_dash'] = true;
BoldThemes_Customize_Default::$data['blog_share_facebook'] = true;
BoldThemes_Customize_Default::$data['blog_share_twitter'] = true;
BoldThemes_Customize_Default::$data['blog_share_linkedin'] = true;
BoldThemes_Customize_Default::$data['pf_grid_gallery_columns'] = '3';
BoldThemes_Customize_Default::$data['pf_grid_gallery_gap'] = 'normal';
BoldThemes_Customize_Default::$data['pf_single_view'] = 'standard';
BoldThemes_Customize_Default::$data['pf_list_view'] = 'standard';
BoldThemes_Customize_Default::$data['pf_share_facebook'] = true;
BoldThemes_Customize_Default::$data['pf_share_twitter'] = true;
BoldThemes_Customize_Default::$data['pf_share_linkedin'] = true;
BoldThemes_Customize_Default::$data['pf_use_dash'] = true;
BoldThemes_Customize_Default::$data['shop_share_facebook'] = true;
BoldThemes_Customize_Default::$data['shop_share_twitter'] = true;
BoldThemes_Customize_Default::$data['shop_share_linkedin'] = true;
BoldThemes_Customize_Default::$data['shop_use_dash'] = true;
BoldThemes_Customize_Default::$data['shop_sale_tag_design'] = 'hard-rounded-top-left';

require_once( get_template_directory() . '/php/after_framework/functions.php' );
require_once( get_template_directory() . '/php/after_framework/customize_params.php' );
 




