<?php

$set = strtolower( basename(__FILE__, '.php') );

$$set = array(
	'worldwide (BusinessAnalytics) ' => $set . '_e900',
	'web (BusinessAnalytics) ' => $set . '_e901',
	'vision (BusinessAnalytics) ' => $set . '_e902',
	'taxes (BusinessAnalytics) ' => $set . '_e903',
	'syncronization (BusinessAnalytics) ' => $set . '_e904',
	'dashboard (BusinessAnalytics) ' => $set . '_e905',
	'skills (BusinessAnalytics) ' => $set . '_e906',
	'shield (BusinessAnalytics) ' => $set . '_e907',
	'Revenue (BusinessAnalytics) ' => $set . '_e908',
	'research (BusinessAnalytics) ' => $set . '_e909',
	'report (BusinessAnalytics) ' => $set . '_e90a',
	'plant (BusinessAnalytics) ' => $set . '_e90b',
	'profit (BusinessAnalytics) ' => $set . '_e90c',
	'people (BusinessAnalytics) ' => $set . '_e90d',
	'pie-chart (BusinessAnalytics) ' => $set . '_e90e',
	'performance (BusinessAnalytics) ' => $set . '_e90f',
	'analytics (BusinessAnalytics) ' => $set . '_e910',
	'analysis1 (BusinessAnalytics) ' => $set . '_e911',
	'search (BusinessAnalytics) ' => $set . '_e912',
	'money (BusinessAnalytics) ' => $set . '_e913',
	'hr (BusinessAnalytics) ' => $set . '_e914',
	'email (BusinessAnalytics) ' => $set . '_e915',
	'document (BusinessAnalytics) ' => $set . '_e916',
	'creative (BusinessAnalytics) ' => $set . '_e917',
	'contract (BusinessAnalytics) ' => $set . '_e918',
	'company (BusinessAnalytics) ' => $set . '_e919',
	'presentation (BusinessAnalytics) ' => $set . '_e91a',
	'business (BusinessAnalytics) ' => $set . '_e91b',
	'analysis (BusinessAnalytics) ' => $set . '_e91c',
	'megaphone (BusinessAnalytics) ' => $set . '_e91d'
);