<?php

$set = strtolower( basename(__FILE__, '.php') );

$$set = array(
	'earth (VirtualReality) ' => $set . '_e900',
	'online-learning (VirtualReality) ' => $set . '_e901',
	'smartphone (VirtualReality) ' => $set . '_e902',
	'movie (VirtualReality) ' => $set . '_e903',
	'virtual (VirtualReality) ' => $set . '_e904',
	'camera (VirtualReality) ' => $set . '_e905',
	'videogame (VirtualReality) ' => $set . '_e906',
	'motion-capture (VirtualReality) ' => $set . '_e907',
	'smartwatch (VirtualReality) ' => $set . '_e908',
	'remote-control (VirtualReality) ' => $set . '_e909',
	'projector (VirtualReality) ' => $set . '_e90a',
	'panoramic (VirtualReality) ' => $set . '_e90b',
	'panorama (VirtualReality) ' => $set . '_e90c',
	'modeling (VirtualReality) ' => $set . '_e90d',
	'controller (VirtualReality) ' => $set . '_e90e',
	'mobile-app (VirtualReality) ' => $set . '_e90f',
	'gamepad (VirtualReality) ' => $set . '_e910',
	'controller1 (VirtualReality) ' => $set . '_e911',
	'interactive (VirtualReality) ' => $set . '_e912',
	'interactive1 (VirtualReality) ' => $set . '_e913',
	'hologram (VirtualReality) ' => $set . '_e914',
	'gloves (VirtualReality) ' => $set . '_e915',
	'glasses (VirtualReality) ' => $set . '_e916',
	'eye-scan (VirtualReality) ' => $set . '_e917',
	'360 (VirtualReality) ' => $set . '_e918',
	'coordination (VirtualReality) ' => $set . '_e919',
	'body-scan (VirtualReality) ' => $set . '_e91a',
	'augmented-reality (VirtualReality) ' => $set . '_e91b',
	'ar-glasses (VirtualReality) ' => $set . '_e91c',
	'3d (VirtualReality) ' => $set . '_e91d'
);