<?php

$set = strtolower( basename(__FILE__, '.php') );

$$set = array(
	'transferring (BusinessMotivation) ' => $set . '_e900',
	'talent-management (BusinessMotivation) ' => $set . '_e901',
	'sharing (BusinessMotivation) ' => $set . '_e902',
	'vision (BusinessMotivation) ' => $set . '_e903',
	'psychology (BusinessMotivation) ' => $set . '_e904',
	'power (BusinessMotivation) ' => $set . '_e905',
	'positive-thinking (BusinessMotivation) ' => $set . '_e906',
	'skills (BusinessMotivation) ' => $set . '_e907',
	'performance (BusinessMotivation) ' => $set . '_e908',
	'partners (BusinessMotivation) ' => $set . '_e909',
	'outstanding (BusinessMotivation) ' => $set . '_e90a',
	'opportunity (BusinessMotivation) ' => $set . '_e90b',
	'mission-accomplished (BusinessMotivation) ' => $set . '_e90c',
	'mind (BusinessMotivation) ' => $set . '_e90d',
	'incentive (BusinessMotivation) ' => $set . '_e90e',
	'generate (BusinessMotivation) ' => $set . '_e90f',
	'inspiration (BusinessMotivation) ' => $set . '_e910',
	'encourage (BusinessMotivation) ' => $set . '_e911',
	'benefit (BusinessMotivation) ' => $set . '_e912',
	'corporate (BusinessMotivation) ' => $set . '_e913',
	'corporate1 (BusinessMotivation) ' => $set . '_e914',
	'corporate2 (BusinessMotivation) ' => $set . '_e915',
	'communication (BusinessMotivation) ' => $set . '_e916',
	'commission (BusinessMotivation) ' => $set . '_e917',
	'career (BusinessMotivation) ' => $set . '_e918',
	'targeting (BusinessMotivation) ' => $set . '_e919',
	'build (BusinessMotivation) ' => $set . '_e91a',
	'boosting-potential (BusinessMotivation) ' => $set . '_e91b',
	'Awaken (BusinessMotivation) ' => $set . '_e91c',
	'partner (BusinessMotivation) ' => $set . '_e91d'
);