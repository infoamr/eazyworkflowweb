<?php

$set = strtolower( basename(__FILE__, '.php') );

$$set = array(
	'world-wide-web (ComputerTechnology) ' => $set . '_e900',
	'webcam (ComputerTechnology) ' => $set . '_e901',
	'smartphone (ComputerTechnology) ' => $set . '_e902',
	'smartphone1 (ComputerTechnology) ' => $set . '_e903',
	'floppy-disc (ComputerTechnology) ' => $set . '_e904',
	'spyware (ComputerTechnology) ' => $set . '_e905',
	'data-server (ComputerTechnology) ' => $set . '_e906',
	'ram (ComputerTechnology) ' => $set . '_e907',
	'projector (ComputerTechnology) ' => $set . '_e908',
	'programming (ComputerTechnology) ' => $set . '_e909',
	'printer (ComputerTechnology) ' => $set . '_e90a',
	'mouse (ComputerTechnology) ' => $set . '_e90b',
	'monitor (ComputerTechnology) ' => $set . '_e90c',
	'videocall (ComputerTechnology) ' => $set . '_e90d',
	'laptop (ComputerTechnology) ' => $set . '_e90e',
	'keyboard (ComputerTechnology) ' => $set . '_e90f',
	'internet (ComputerTechnology) ' => $set . '_e910',
	'hard-drive (ComputerTechnology) ' => $set . '_e911',
	'fingerprint (ComputerTechnology) ' => $set . '_e912',
	'file (ComputerTechnology) ' => $set . '_e913',
	'file1 (ComputerTechnology) ' => $set . '_e914',
	'domain (ComputerTechnology) ' => $set . '_e915',
	'digital (ComputerTechnology) ' => $set . '_e916',
	'data (ComputerTechnology) ' => $set . '_e917',
	'data1 (ComputerTechnology) ' => $set . '_e918',
	'cpu (ComputerTechnology) ' => $set . '_e919',
	'copyright (ComputerTechnology) ' => $set . '_e91a',
	'computer (ComputerTechnology) ' => $set . '_e91b',
	'bandwith (ComputerTechnology) ' => $set . '_e91c',
	'antivirus (ComputerTechnology) ' => $set . '_e91d'
);