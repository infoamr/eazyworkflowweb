<?php

$set = strtolower( basename(__FILE__, '.php') );

$$set = array(
	'vision (AgileMethodology) ' => $set . '_e900',
	'speed (AgileMethodology) ' => $set . '_e901',
	'test (AgileMethodology) ' => $set . '_e902',
	'time (AgileMethodology) ' => $set . '_e903',
	'vote (AgileMethodology) ' => $set . '_e904',
	'team-member (AgileMethodology) ' => $set . '_e905',
	'team (AgileMethodology) ' => $set . '_e906',
	'task-list (AgileMethodology) ' => $set . '_e907',
	'stakeholder (AgileMethodology) ' => $set . '_e908',
	'planning (AgileMethodology) ' => $set . '_e909',
	'planning1 (AgileMethodology) ' => $set . '_e90a',
	'objective (AgileMethodology) ' => $set . '_e90b',
	'team1 (AgileMethodology) ' => $set . '_e90c',
	'role (AgileMethodology) ' => $set . '_e90d',
	'meeting (AgileMethodology) ' => $set . '_e90e',
	'release (AgileMethodology) ' => $set . '_e90f',
	'quality-assurance (AgileMethodology) ' => $set . '_e910',
	'id-card (AgileMethodology) ' => $set . '_e911',
	'obstacle (AgileMethodology) ' => $set . '_e912',
	'feedback (AgileMethodology) ' => $set . '_e913',
	'search (AgileMethodology) ' => $set . '_e914',
	'software-development (AgileMethodology) ' => $set . '_e915',
	'development (AgileMethodology) ' => $set . '_e916',
	'developer (AgileMethodology) ' => $set . '_e917',
	'analyst (AgileMethodology) ' => $set . '_e918',
	'chart (AgileMethodology) ' => $set . '_e919',
	'report (AgileMethodology) ' => $set . '_e91a',
	'agile (AgileMethodology) ' => $set . '_e91b',
	'testing (AgileMethodology) ' => $set . '_e91c',
	'acceptance (AgileMethodology) ' => $set . '_e91d'
);