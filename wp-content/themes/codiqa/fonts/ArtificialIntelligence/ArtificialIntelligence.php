<?php

$set = strtolower( basename(__FILE__, '.php') );

$$set = array(
	'virtual-reality (ArtificialIntelligence) ' => $set . '_e900',
	'assistant (ArtificialIntelligence) ' => $set . '_e901',
	'brainstorming (ArtificialIntelligence) ' => $set . '_e902',
	'turing (ArtificialIntelligence) ' => $set . '_e903',
	'smartphone (ArtificialIntelligence) ' => $set . '_e904',
	'fingerprint-scan (ArtificialIntelligence) ' => $set . '_e905',
	'eye-scan (ArtificialIntelligence) ' => $set . '_e906',
	'robot (ArtificialIntelligence) ' => $set . '_e907',
	'research (ArtificialIntelligence) ' => $set . '_e908',
	'learning (ArtificialIntelligence) ' => $set . '_e909',
	'funnel (ArtificialIntelligence) ' => $set . '_e90a',
	'nanotechnology (ArtificialIntelligence) ' => $set . '_e90b',
	'brain1 (ArtificialIntelligence) ' => $set . '_e90c',
	'world (ArtificialIntelligence) ' => $set . '_e90d',
	'invention (ArtificialIntelligence) ' => $set . '_e90e',
	'creativity (ArtificialIntelligence) ' => $set . '_e90f',
	'hologram (ArtificialIntelligence) ' => $set . '_e910',
	'machine (ArtificialIntelligence) ' => $set . '_e911',
	'drone (ArtificialIntelligence) ' => $set . '_e912',
	'database (ArtificialIntelligence) ' => $set . '_e913',
	'cpu (ArtificialIntelligence) ' => $set . '_e914',
	'binary-code (ArtificialIntelligence) ' => $set . '_e915',
	'cloud-computing (ArtificialIntelligence) ' => $set . '_e916',
	'microphone (ArtificialIntelligence) ' => $set . '_e917',
	'brain (ArtificialIntelligence) ' => $set . '_e918',
	'artificial-heart (ArtificialIntelligence) ' => $set . '_e919',
	'robotic-arm (ArtificialIntelligence) ' => $set . '_e91a',
	'ar (ArtificialIntelligence) ' => $set . '_e91b',
	'analytics (ArtificialIntelligence) ' => $set . '_e91c',
	'AI (ArtificialIntelligence) ' => $set . '_e91d'
);